// Packages
import * as Client from "pg";

export default function queryResolver (client: Client.Client, queryString: string, params: unknown[] = []): Promise<any> {

	return new Promise((resolve, reject) => {
		client.query(queryString, params,
		(error, results) => {
			if (error) reject(error);

			resolve(results);
		});
	});
}