// Packages
import * as Client from "mysql2";

export default async function queryResolver (client: Client.Connection, queryString: string, params: unknown[] = []): Promise<any> {
	let result;

	try {
		result = await new Promise((resolve, reject) => {
			client.query(queryString, params,
			(error, results) => {
				if (error) reject(error);
	
				resolve(results);
			});
		});
	}
	catch (e) {
		if (e.sqlMessage || e.sqlState) {
			e.query = queryString;
			throw e;
		}
	}

	return result;
}