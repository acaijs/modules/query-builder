// -------------------------------------------------
// Imports
// -------------------------------------------------

import AbstractQuery 	from "./src/abstractions/builder";
import SqlQuery 		from './src/classes/queryStrategies/sql';
import PgQuery 			from './src/classes/queryStrategies/sql';

// interfaces
import ModelContent from "./src/interfaces/ModelContent";

// -------------------------------------------------
// Configurations
// -------------------------------------------------

const queries = {} as Record<string, typeof AbstractQuery>;

export async function addQuery (name: string, type: string, config?: Record<string, ModelContent>) {
	switch (type) {
		case "sql":
		case "mysql":
		case "mysqli":
			queries[name] = SqlQuery;
		break;
		case "pg":
		case "postgres":
		case "postgresql":
			queries[name] = PgQuery;
		break;
	}

	if (config) {
		await queries[name].toggleSettings(config);
	}
}

export async function setDefault(name:string, config?: Record<string, ModelContent>) {
	await addQuery("default", name, config);
}

// -------------------------------------------------
// Exports
// -------------------------------------------------

// Base abstract query
export {default as AbstractQuery} 	from './src/abstractions/builder';

// Implementations
export {default as SqlQuery} 		from './src/classes/queryStrategies/sql';
export {default as PgQuery} 		from './src/classes/queryStrategies/sql';

// default query
export default (key?: string): typeof AbstractQuery => queries[key || "default"];